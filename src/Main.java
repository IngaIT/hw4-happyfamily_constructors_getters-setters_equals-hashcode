
public class Main {
    public static void main(String[] args) {

        Pet dog = new Pet("собака", "Барбос", 5, 75, new String[] {"їсть", "грається", "спить"});
        Pet cat = new Pet("кіт", "Мурзік");

        Human father1 = new Human("Олег", "Іванов", 1975, 130, new String[][] {{"понеділок", "робота"}, {"вівторок", "робота"}});
        Human mother1 = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);

        Human father2 = new Human("Андрій", "Петров", 1980);
        Human mother2 = new Human("Наталія", "Петрова", 1982);
        Human child2 = new Human("Максим", "Петров", 2005, 100, new String[][] {{"середа", "школа"}, {"четвер", "школа"}});

        Family family1 = new Family(mother1, father1);
        family1.setPet(dog);
        family1.addChild(child1);

        Family family2 = new Family(mother2, father2);
        family2.setPet(cat);
        family2.addChild(child2);

        System.out.println(family1);
        System.out.println(family2);

        child1.greetPet();
        child1.describePet();

        child2.greetPet();
        child2.describePet();

        dog.eat();
        dog.respond();
        dog.foul();

        cat.eat();
        cat.respond();
        cat.foul();

        System.out.println(family1.countFamily());
        System.out.println(family2.countFamily());

        family1.deleteChild(0);
        family2.deleteChild(0);

        System.out.println(family1);
        System.out.println(family2);

        System.out.println(family1.countFamily());
        System.out.println(family2.countFamily());
    }
}
